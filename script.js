function createShowList(array) {

    let list = document.createElement("ul");
    document.body.append(list);
    list.id = "list";
    
    let content = array.map((element) => { return `<li>${element}</li>` }).join("");
    document.getElementById("list").innerHTML = content;

}
createShowList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
